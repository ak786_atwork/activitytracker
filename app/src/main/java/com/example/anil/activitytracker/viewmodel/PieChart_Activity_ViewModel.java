package com.example.anil.activitytracker.viewmodel;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.ViewModel;
import android.graphics.Color;
import android.util.Log;

import com.example.anil.activitytracker.CustomApplication;
import com.example.anil.activitytracker.analysis_view_classes.PieChartHelper;
import com.example.anil.activitytracker.database.DailyActivityTime;
import com.example.anil.activitytracker.repository.DataRepository;

import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Random;

public class PieChart_Activity_ViewModel extends ViewModel {

    private DataRepository dataRepository;
    private  ArrayList<PieChartHelper> PieChartHelperArrayList;

    DecimalFormat decimalFormat ;

    public PieChart_Activity_ViewModel()
    {
        dataRepository = new DataRepository();
        PieChartHelperArrayList = new ArrayList<>();

    }

    public ArrayList<PieChartHelper> getPieChartHelperArrayList()
    {
        return PieChartHelperArrayList;
    }
    public LiveData<List<DailyActivityTime>> getDataBetweenDates(Date minValue, Date maxValue)
    {
        return dataRepository.getDataBetweenDates(minValue,maxValue);
    }
    public LiveData<DailyActivityTime> findDataByDate(Date date)
    {
        return  dataRepository.findDataByDate(date);
    }

    public LiveData<List<DailyActivityTime>> getAllData()
    {
        return dataRepository.getAllData();

    }


    public HashSet<Integer> getRandomColor(int size)
    {
        int count = 0;
        HashSet<Integer> colorSet = new HashSet<>();

        while (count < size)
        {
            colorSet.add(getRandomColor());
            count = colorSet.size();
        }

        return colorSet;
    }

    public int getRandomColor()
    {
        Random random = new Random();
        int r = random.nextInt(255);
        int g = random.nextInt(255);
        int b = random.nextInt(255);

        return Color.rgb(r,g,b);
    }

    private ArrayList<PieChartHelper> preparePieChartWithActivityHashmap(HashMap<String,Double> hashMap)
    {

        //clear the arraylist first
        PieChartHelperArrayList.clear();

        HashSet<Integer> colorHashset = getRandomColor(hashMap.size());
        Integer[] colorArray = new Integer[colorHashset.size()];
        colorHashset.toArray(colorArray);
        showLog(colorArray.toString());


        int colorIndex = 0;
        //int totalMinutes = 0;
        double totalHours = 0;

        for (Map.Entry<String,Double> entry : hashMap.entrySet())
        {
            totalHours = entry.getValue();
            PieChartHelper PieChartHelper = new PieChartHelper();
            PieChartHelper.color = (int)colorArray[colorIndex++];
            PieChartHelper.key = entry.getKey();
            PieChartHelper.totalHours = roundOff(entry.getValue());
            PieChartHelper.degree = getDegreeFromHours(totalHours);
            PieChartHelper.percent = (float) roundOff(getPercentFromHours(totalHours));

            PieChartHelperArrayList.add(PieChartHelper);

        }
        return PieChartHelperArrayList;
    }
    public ArrayList<PieChartHelper> prepareChoicesData(DailyActivityTime dailyActivityTime)
    {

        //HashMap<String,Integer> hashMap = dailyActivityTime.activityHashMap;

        return preparePieChartWithActivityHashmap(dailyActivityTime.activityHashMap);
    }

/*    private float convertMinutesIntoHour(int mins)
    {
        return (float)(mins/60);
    }*/
    public float dpToPixal(float dp)
    {
        float density = CustomApplication.getInstance().getResources().getDisplayMetrics().density;
        return dp * density;
    }


    private float getPercentFromHours(double totalHours) {
        //conversion =  24 * 60 = 100 percent
        // now hours rate implemented
        // 24 hrs = 100 percent
       // float conversionConst = (float)(100/(24 * 60));
        double conversionConst = 100/24;
        return (float)(conversionConst * totalHours);
    }

    private float getDegreeFromHours(double totalHours) {
        //returning in reference with 24 hrs
        // 24 hrs = 360   --> 1hr = 360/24
        // 1 min = 0.25 degree
        return (float)(360/24 * totalHours);
    }

    public ArrayList<PieChartHelper> prepareChoicesData(List<DailyActivityTime> dailyActivityTimes)
    {
        HashMap<String, Double> normalizedDailyActivityTimesHashmap = new HashMap<>();
        int totalDays = dailyActivityTimes.size();
        for (DailyActivityTime dailyActivityTime :dailyActivityTimes)
        {
            HashMap<String, Double> hashMap = dailyActivityTime.activityHashMap;
            for (Map.Entry<String,Double> entry : hashMap.entrySet())
            {
                //add the
                //don't just add first normalize and then add future
                //find average
                if (normalizedDailyActivityTimesHashmap.get(entry.getKey()) != null)
                {
                    normalizedDailyActivityTimesHashmap.put(entry.getKey(),entry.getValue() + getAverage(entry.getValue(),totalDays));
                }
                else
                {
                    normalizedDailyActivityTimesHashmap.put(entry.getKey(),getAverage(entry.getValue(),totalDays));
                }

            }
        }
        //yet to write code
        //now it is in requires format
        return preparePieChartWithActivityHashmap(normalizedDailyActivityTimesHashmap);
    }

    // #.##
    public double roundOff(double doubleValue)
    {
        decimalFormat = new DecimalFormat("#.##");
        decimalFormat.setRoundingMode(RoundingMode.CEILING);

        return Double.parseDouble(decimalFormat.format(doubleValue));
    }

 /*   private HashMap<String, Integer> convertListIntoStandardForm(HashMap<String, Double> normalizedDailyActivityTimesHashmap)
    {
        HashMap<String, Integer> hashMap = new HashMap<>();
        for (Map.Entry<String,Double> entry :normalizedDailyActivityTimesHashmap.entrySet())
        {
            hashMap.put(entry.getKey(),entry.getValue().intValue());
        }
        return hashMap;
    }
*/
    private void showLog(String info)
    {
        Log.d("PieChart_Activity_ViewModel", info);
    }

    //double totalDays  ,.  as i need the data returned in double and if i call this method with int argument it will
    //internally upcast it and i will get the result in double
    public Double getAverage(double totalHours, double totalDays) {
        showLog("average = "+totalHours/totalDays);
        return totalHours/totalDays;
    }



}

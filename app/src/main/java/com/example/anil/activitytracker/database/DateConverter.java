package com.example.anil.activitytracker.database;

import android.arch.persistence.room.TypeConverter;
import android.util.Log;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DateConverter {

    @TypeConverter
    public static String fromDate(Date date) {
        if (date==null) {
            return(null);
        }

        Log.d("date " ," "+android.text.format.DateFormat.format("yyyy-MM-dd", date).toString());

        //2019-01-18
        return android.text.format.DateFormat.format("yyyy-MM-dd", date).toString();
    }

    @TypeConverter
    public static Date toDate(String dateInString) {
        if (dateInString == null) {
            return(null);
        }
        Date date = null;
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        try {
             date = simpleDateFormat.parse(dateInString);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        //Log.d("date "," date = "+date.getDate());
        return date;
    }
}

package com.example.anil.activitytracker.viewmodel;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.ViewModel;
import android.icu.util.LocaleData;
import android.util.Log;

import com.example.anil.activitytracker.CustomApplication;
import com.example.anil.activitytracker.database.DailyActivityTime;
import com.example.anil.activitytracker.repository.DataRepository;

import java.util.Date;
import java.util.HashMap;
import java.util.List;

public class TableView_Activity_ViewModel extends ViewModel {

    private DataRepository dataRepository;

    public TableView_Activity_ViewModel()
    {
        dataRepository = new DataRepository();
    }
    public LiveData<List<DailyActivityTime>> getDataBetweenDates(Date minValue, Date maxValue)
    {
        return dataRepository.getDataBetweenDates(minValue,maxValue);
    }
    public LiveData<DailyActivityTime> findDataByDate(Date date)
    {
        return  dataRepository.findDataByDate(date);
    }

    public LiveData<List<DailyActivityTime>> getAllData()
    {
        return dataRepository.getAllData();

    }


    public void printActivityHashmap(List<DailyActivityTime> dailyActivityTimes)
    {
        for (DailyActivityTime dailyActivityTime : dailyActivityTimes)
        {
            printActivityHashmap(dailyActivityTime.activityHashMap);
        }
    }

    public void printActivityHashmap(HashMap<String, Double> hashMap)
    {
        showLog("lists "+hashMap.toString());
    }

    private void showLog(String s) {
        Log.d(" TABLE VIEW MODEL " ,s);
    }

    public  String fromDate(Date date) {
        if (date==null) {
            return(null);
        }

        Log.d("date " ," "+android.text.format.DateFormat.format("yyyy-MM-dd", date).toString());

        //2019-01-18
        return android.text.format.DateFormat.format("yyyy-MM-dd", date).toString();
    }

    public float dpToPixal(float dp)
    {
        float density = CustomApplication.getInstance().getResources().getDisplayMetrics().density;
        return dp * density;
    }

    public float pixalToDp(float px)
    {
        float density = CustomApplication.getInstance().getResources().getDisplayMetrics().density;
        return px / density;
    }
}

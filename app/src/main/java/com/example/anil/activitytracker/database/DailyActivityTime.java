package com.example.anil.activitytracker.database;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.arch.persistence.room.TypeConverters;
import android.support.annotation.NonNull;

import java.util.Date;
import java.util.HashMap;

@Entity
public class DailyActivityTime {


    @PrimaryKey
    @NonNull
    public Date date;

    @ColumnInfo
    public HashMap<String,Double> activityHashMap;

    public DailyActivityTime(@NonNull Date date, HashMap<String,Double> activityHashMap) {
        this.date = date;
        this.activityHashMap = activityHashMap;
    }
}

package com.example.anil.activitytracker.testing;

import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;


import com.example.anil.activitytracker.CustomApplication;
import com.example.anil.activitytracker.FlowLayout;
import com.example.anil.activitytracker.R;
import com.example.anil.activitytracker.TagLayout;

public class FlowLayoutTestingActivity extends AppCompatActivity {

    private FlowLayout flowLayout;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_flow_layout_testing);

        flowLayout = (FlowLayout) findViewById(R.id.flowLayout);

        addTextView();
    }

    private void addTextView() {

        for (int i =0 ;i<30; i++)
        {
            TextView textView = new TextView(FlowLayoutTestingActivity.this);
            textView.setText("tag "+i +" ");
            textView.setTextColor(Color.BLACK);
            textView.setTextSize(dpToPixal(15));
            textView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //v.setVisibility(View.GONE);
                    flowLayout.removeView(v);
                }
            });
            flowLayout.addView(textView);

        }
    }

    public float dpToPixal(float dp)
    {
        float density = CustomApplication.getInstance().getResources().getDisplayMetrics().density;
        return dp * density;
    }
}

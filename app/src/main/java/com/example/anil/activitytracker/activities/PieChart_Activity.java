package com.example.anil.activitytracker.activities;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.support.annotation.Nullable;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.example.anil.activitytracker.CustomApplication;
import com.example.anil.activitytracker.R;
import com.example.anil.activitytracker.analysis_view_classes.PieChart;
import com.example.anil.activitytracker.analysis_view_classes.PieChartHelper;
import com.example.anil.activitytracker.database.DailyActivityTime;
import com.example.anil.activitytracker.viewmodel.PieChart_Activity_ViewModel;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

public class PieChart_Activity extends AppCompatActivity {

    private String TAG = "PIECHARTACTIVITY";

    private PieChart pieChart;
    private long fromTime, toTime;
    private PieChart_Activity_ViewModel viewModel;
    private NestedScrollView nestedScrollView;
    private LinearLayout scrollViewChild;

    private int dataRequestCategory = -1;
    LinearLayout parentLinearLayout;
    RelativeLayout parentRelativeLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        parentRelativeLayout = (RelativeLayout) LayoutInflater.from(this).inflate(R.layout.activity_pie_chart_,null);
        setContentView(parentRelativeLayout);


        init();

    }
    public void onClick(View view)
    {
        pieChart.drawAgain();
    }
    private void init() {

        nestedScrollView = findViewById(R.id.nested_scroll_view);
        scrollViewChild = findViewById(R.id.scroll_view_child_layout);
        //setting viewModel
        viewModel = ViewModelProviders.of(this).get(PieChart_Activity_ViewModel.class);
        pieChart = new PieChart(this);
        pieChart.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, (int)viewModel.dpToPixal(350)));
        //parentLinearLayout.addView(pieChart,1);
        parentRelativeLayout.addView(pieChart,0);



        Intent intent = getIntent();
        fromTime = intent.getLongExtra("fromDate",-1);
        toTime = intent.getLongExtra("toDate",-1);
        dataRequestCategory =  intent.getIntExtra("dataRequestCategory",-1);

        makeDataRequest(fromTime,toTime,dataRequestCategory);

        showLog(fromTime+" time == "+toTime);
    }

    // 0 - single date request
    // 1 - between dates
    // 2 - means all data

    private void makeDataRequest(long fromTime, long toTime, final int dataRequestCategory) {

        switch (dataRequestCategory)
        {
            case 0 :
                //get single date data
                viewModel.findDataByDate(new Date(fromTime)).observe(this, new Observer<DailyActivityTime>() {
                    @Override
                    public void onChanged(@Nullable DailyActivityTime dailyActivityTime) {
                        if (dailyActivityTime != null)
                        {
                            pieChart.setPieChartHelperArrayList(viewModel.prepareChoicesData(dailyActivityTime));
                            pieChart.drawAgain();
                            //now viewmodel has set the helper arraylist now i can get
                            addTextViewToScrollView(viewModel.getPieChartHelperArrayList());
                            showLog("dataRequestCategory = "+dataRequestCategory+" key = date = "+dailyActivityTime.date);
                        }

                    }
                });
                break;

            case 1 :
                //get data between dates
                viewModel.getDataBetweenDates(new Date(fromTime),new Date(toTime)).observe(this, new Observer<List<DailyActivityTime>>() {
                    @Override
                    public void onChanged(@Nullable List<DailyActivityTime> dailyActivityTimes) {
                        if (dailyActivityTimes != null)
                        {
                            pieChart.setPieChartHelperArrayList(viewModel.prepareChoicesData(dailyActivityTimes));
                            pieChart.drawAgain();
                            //now viewmodel has set the helper arraylist now i can get
                            addTextViewToScrollView(viewModel.getPieChartHelperArrayList());
                            showLog(" "+dataRequestCategory+" key = date = "+ dailyActivityTimes.size());
                        }

                    }
                });
                break;

            case 2 :
                //get all data
                viewModel.getAllData().observe(this, new Observer<List<DailyActivityTime>>() {
                    @Override
                    public void onChanged(@Nullable List<DailyActivityTime> dailyActivityTimes) {
                        if (dailyActivityTimes != null)
                        {
                            //setting in piechart helper class
                            pieChart.setPieChartHelperArrayList(viewModel.prepareChoicesData(dailyActivityTimes));
                            pieChart.drawAgain();
                            //now viewmodel has set the helper arraylist now i can get
                            addTextViewToScrollView(viewModel.getPieChartHelperArrayList());
                            showLog(" "+dataRequestCategory+" key = date = "+ dailyActivityTimes.size());
                        }
                    }
                });
                break;
        }
    }

    private void addTextViewToScrollView(ArrayList<PieChartHelper> pieChartHelperArrayList) {

        //StringBuffer stringBuffer = new StringBuffer();
        String label = "";
        for (PieChartHelper helper : pieChartHelperArrayList)
        {
            //   stringBuffer.append("total hours spent = "+helper.totalHours + " ->"+helper.percent);

            label = helper.key + " -> "+helper.totalHours + "hrs ->"+helper.percent+"%";
            LinearLayout linearLayout = (LinearLayout) LayoutInflater.from(this).inflate(R.layout.piechart_row_item, null);

            TextView  textView = linearLayout.findViewWithTag("textview");
            ImageView imageView = linearLayout.findViewWithTag("imageview");

            textView.setText(label);

            imageView.setBackgroundColor(helper.color);
            //checking color
           /* if (helper.color == Color.DKGRAY)
            {
                textView.setBackgroundColor(findDifferentColor(helper.color));
            }
            else
            {
                textView.setBackgroundColor(helper.color);
            }*/

            //add textview to scroll_view_child_layout
            scrollViewChild.addView(linearLayout);
        }
    }

    //earlier
/*    private void addTextViewToScrollView(ArrayList<PieChartHelper> pieChartHelperArrayList) {

        //StringBuffer stringBuffer = new StringBuffer();
        String label = "";
        for (PieChartHelper helper : pieChartHelperArrayList)
        {
         //   stringBuffer.append("total hours spent = "+helper.totalHours + " ->"+helper.percent);

            label = helper.key + " -> total hours spent = "+helper.totalHours + " ->"+helper.percent;
            TextView  textView = createTextView();
            textView.setText(label);

            //checking color
            if (helper.color == Color.DKGRAY)
            {
                textView.setBackgroundColor(findDifferentColor(helper.color));
            }
            else
            {
             textView.setBackgroundColor(helper.color);
            }

            //add textview to scroll_view_child_layout
            scrollViewChild.addView(textView);
        }
    }*/

    private int findDifferentColor(int color) {
        while (true)
        {
            int newColor = viewModel.getRandomColor();
            if (newColor != color)
                return newColor;
        }
    }

    private void printActivityHashmap(List<HashMap<String, Double>> hashMap)
    {
        for (HashMap<String, Double> stringDoubleHashMap : hashMap)
        {
            printActivityHashmap(stringDoubleHashMap);
        }
    }

    private void printActivityHashmap(HashMap<String, Double> hashMap)
    {
        showLog("lists "+hashMap.toString());
    }

    private TextView  createTextView()
    {
        //set the style and listener here
        TextView textView = new TextView(this);
        textView.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
//        textView.setMaxLines(2);
//        textView.setEllipsize(TextUtils.TruncateAt.END);
        textView.setPadding(5,5,5,5);
        textView.setTextSize(20);
        textView.setTypeface(Typeface.DEFAULT,Typeface.BOLD);
        textView.setTextColor(Color.DKGRAY);
        textView.setGravity(View.TEXT_ALIGNMENT_CENTER);
        int padding = (int) viewModel.dpToPixal(5);
        textView.setPadding(padding,padding,padding,padding);
        return textView;
    }



    @Override
    protected void onResume() {
        super.onResume();


    }

    public  void showLog(String info)
    {
        Log.d(TAG, info);
    }
}

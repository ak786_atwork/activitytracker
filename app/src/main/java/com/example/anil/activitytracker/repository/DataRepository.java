package com.example.anil.activitytracker.repository;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.os.AsyncTask;

import com.example.anil.activitytracker.CustomApplication;
import com.example.anil.activitytracker.database.AppDatabase;
import com.example.anil.activitytracker.database.DailyActivityTime;
import com.example.anil.activitytracker.database.MyDao;

import java.util.Date;
import java.util.List;

public class DataRepository {

    AppDatabase db;
    static MyDao myDao;

    private static List<DailyActivityTime> list;

    private MutableLiveData<List<DailyActivityTime>> mutableLiveDataList ;
    private MutableLiveData<DailyActivityTime> dailyActivityTimeMutableLiveData;

    public DataRepository()
    {
        db = AppDatabase.getDatabase(CustomApplication.getInstance());
        myDao = db.getDao();

        mutableLiveDataList = new MutableLiveData<>();
        dailyActivityTimeMutableLiveData = new MutableLiveData<>();


    }
    public  void deleteAll()
    {
        new Thread(new Runnable() {
            @Override
            public void run() {
                myDao.deleteAll();
            }
        }).start();
    }

    public MutableLiveData<List<DailyActivityTime>> getMutableLiveDataList() {
        return mutableLiveDataList;
    }

    public MutableLiveData<DailyActivityTime> getDailyActivityTimeMutableLiveData() {
        return dailyActivityTimeMutableLiveData;
    }

    public void insert(final DailyActivityTime dailyActivityTime)
    {

        new Thread(new Runnable() {
            @Override
            public void run() {
                myDao.insertTiming(dailyActivityTime);
            }
        }).start();
    }

    public void update(final DailyActivityTime dailyActivityTime)
    {
        new Thread(new Runnable() {
            @Override
            public void run() {
                myDao.updateTiming(dailyActivityTime);
            }
        }).start();

    }

    public LiveData<List<DailyActivityTime>> getAllData()
    {
       return myDao.getAll();
         /*GetAllData getAllData = new GetAllData();
         getAllData.execute();*/
    }

    /*static class GetAllData extends AsyncTask<Void, Void, List<DailyActivityTime>> {
        @Override
        protected List<DailyActivityTime> doInBackground(Void... voids) {
            return myDao.getAll();
        }

        @Override
        protected void onPostExecute(List<DailyActivityTime> dailyActivityTimes) {
            list = dailyActivityTimes;
        }
    };*/

/*    public List<DailyActivityTime> getDataBetweenDates(Date minValue, Date maxValue)
    {
        final List<DailyActivityTime> list = null;
        new Thread(new Runnable() {
            @Override
            public void run() {
               list =  myDao.insertTiming(dailyActivityTime);
            }
        }).start();
        return  myDao.findDataBetweenDates(minValue,maxValue);
    }*/

    public LiveData<DailyActivityTime> findDataByDate(Date date)
    {
        return  myDao.findDataByDate(date);
    }

    public LiveData<List<DailyActivityTime>> getDataBetweenDates(Date minValue, Date maxValue)
    {
        return myDao.findDataBetweenDates(minValue,maxValue);
    }
}

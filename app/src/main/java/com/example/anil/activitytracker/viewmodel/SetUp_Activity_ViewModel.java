package com.example.anil.activitytracker.viewmodel;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.ViewModel;

import com.example.anil.activitytracker.database.DailyActivityTime;
import com.example.anil.activitytracker.repository.DataRepository;

import java.util.Date;
import java.util.List;

public class SetUp_Activity_ViewModel extends ViewModel {

    private DataRepository dataRepository;

    public SetUp_Activity_ViewModel()
    {
        dataRepository = new DataRepository();
    }

    public DataRepository getDataRepository() {
        return dataRepository;
    }


    public LiveData<List<DailyActivityTime>> getDataBetweenDates(Date minValue, Date maxValue)
    {
        return dataRepository.getDataBetweenDates(minValue,maxValue);
    }
    public LiveData<DailyActivityTime> findDataByDate(Date date)
    {
        return  dataRepository.findDataByDate(date);
    }

    public LiveData<List<DailyActivityTime>> getAllData()
    {
        return dataRepository.getAllData();

    }
}

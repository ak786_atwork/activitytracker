package com.example.anil.activitytracker;

import android.content.Context;
import android.content.SharedPreferences;

import java.util.HashSet;
import java.util.Set;

public class SharedPreferences_StoringChoices {

    SharedPreferences sharedPreferences;
    private final String CHOICE_PREF = "CHOICE_PREF";

    public SharedPreferences_StoringChoices()
    {
        sharedPreferences = CustomApplication.getInstance().getSharedPreferences(CHOICE_PREF,Context.MODE_PRIVATE);
    }

    public void storeChoices(HashSet<String> choicesHashSet)
    {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putStringSet("choicesHashSet", choicesHashSet);
        editor.commit();
       /* int size = choicesHashSet.size();
        String[] choices = new String[size];
        int sizeCount = 0;

        for (String s : choicesHashSet)
        {
            choices[sizeCount++] = s;
        }*/

    }

    public HashSet<String> getChoicesHashSet()
    {
        return (HashSet<String>) sharedPreferences.getStringSet("choicesHashSet",new HashSet<String>());
    }

    public void storeLaunchBoolean(boolean choicesFinalized)
    {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean("choicesFinalized", choicesFinalized);
        editor.commit();
    }

    public boolean getLaunchBoolean()
    {
        return sharedPreferences.getBoolean("choicesFinalized",false);
    }
}

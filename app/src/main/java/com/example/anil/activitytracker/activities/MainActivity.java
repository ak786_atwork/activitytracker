package com.example.anil.activitytracker.activities;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModel;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.anil.activitytracker.R;
import com.example.anil.activitytracker.TagLayout;
import com.example.anil.activitytracker.databinding.ActivityMainBinding;
import com.example.anil.activitytracker.viewmodel.MainActivity_ViewModel;

import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    TagLayout flowLayout ;
    MainActivity_ViewModel mainActivity_viewModel;

    private String TAG = "MAIN_ACTIVITY";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mainActivity_viewModel = ViewModelProviders.of(this).get(MainActivity_ViewModel.class);

        ActivityMainBinding activityMainBinding = DataBindingUtil.setContentView(this,R.layout.activity_main);
        activityMainBinding.setMainActivityViewModel(mainActivity_viewModel);

        // must set this with live data
        //To use a LiveData object with your binding class, you need to specify a lifecycle owner to define the scope of the LiveData object.
        activityMainBinding.setLifecycleOwner(this );

        activityMainBinding.executePendingBindings();


        flowLayout = findViewById(R.id.flowLayout);

        addSelectedChoice();

        Date date = new Date();
        Log.d(TAG, " date = "+date.getDate()+" time"+date.getTime());


    }

    public void addSelectedChoice()
    {
        mainActivity_viewModel.getSelectedChoice().observe(this, new Observer<MainActivity_ViewModel.ViewStateHolder>() {
            @Override
            public void onChanged(@Nullable MainActivity_ViewModel.ViewStateHolder viewStateHolder) {

                TextView textView = (TextView)LayoutInflater.from(MainActivity.this).inflate(R.layout.selected_choice_item_textview,null);
                textView.setText(viewStateHolder.key);
                textView.setTag(viewStateHolder.key);
                textView.setId(viewStateHolder.id);
                textView.setOnClickListener(MainActivity.this);


                flowLayout.addView(textView);
                //earlier code
               /* Log.d(TAG, "observer called");
                String s = viewStateHolder.key;
                if (viewStateHolder.addedInMap)
                {
                    //just make it visible again
                    TextView textView1 = getTextViewByTag(flowLayout, s);
                    if (textView1 != null)
                    {
                        textView1.setVisibility(View.VISIBLE);
                    }
                }
                else
                {
                    TextView textView = (TextView)LayoutInflater.from(MainActivity.this).inflate(R.layout.selected_choice_item_textview,null);
                    textView.setText(s);
                    textView.setTag(s);
                    textView.setOnClickListener(MainActivity.this);
                    //textView.setVisibility(View.VISIBLE);

                    flowLayout.addView(textView);
                }*/

            }
        });
    }

    public void addDemoView()
    {
        LayoutInflater layoutInflater = getLayoutInflater();
        String tag;
        for (int i = 0; i <= 20; i++) {
            tag = "#tag" + i;
            View tagView = layoutInflater.inflate(R.layout.item_layout, null, false);

            final TextView tagTextView =  tagView.findViewById(R.id.tagTextView);
            tagTextView.setText(tag);

            tagTextView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    tagTextView.setVisibility(View.GONE);
                }
            });

            flowLayout.addView(tagView);
        }
    }

    public TextView getTextViewByTag(ViewGroup flowlayout, String tag)
    {
        String childTag = "";
        for (int i = 0 ;i<flowlayout.getChildCount(); i++)
        {
            childTag = (String) flowlayout.getChildAt(i).getTag();
            if (childTag.equals(tag))
            {
                return (TextView) flowlayout.getChildAt(i);
            }
        }
        return null;
    }
    public void onBtnClicked(View view)
    {
        int id = view.getId();
        Intent intent = null;

        switch (id)
        {
            case R.id.setup_btn:
                //
                intent = new Intent(MainActivity.this, SetUp_Activity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                intent.putExtra("choices",new HashSet<>(mainActivity_viewModel.getListOfChoices()));
                break;

            case R.id.add_activites_btn:
                //
                intent = new Intent(MainActivity.this, AddChoice_Activity.class);
                intent.putExtra("choices",new HashSet<>(mainActivity_viewModel.getListOfChoices()));
                break;

            case R.id.create_own_set_btn:
                //
                intent = new Intent(MainActivity.this, AddChoice_Activity.class);
                break;

        }
        startActivity(intent);
    }

    @Override
    public void onClick(View v) {

        //get parent layout from view state holder and create textview and add to parent layout and remove from here
        MainActivity_ViewModel.ViewStateHolder viewStateHolder = mainActivity_viewModel.getViewStateHolderById(v.getId());

        TextView textView = (TextView)LayoutInflater.from(MainActivity.this).inflate(R.layout.selected_choice_item_textview,null);
        textView.setText(viewStateHolder.key);
        textView.setTag(viewStateHolder.key);
        textView.setId(viewStateHolder.id);

        viewStateHolder.parentFlowLayout.addView(textView);
        //remove this view from here
        flowLayout.removeView(v);

        textView.setOnClickListener(mainActivity_viewModel);

        //earlier
        /*Log.d(TAG, "onclick in main activity");
        String  tag = (String) v.getTag();
        //hide this view

        //unhide the same data view in choice options
        int id = mainActivity_viewModel.getViewId(tag);
        if (id != -1)
        {
            findViewById(id).setVisibility(View.VISIBLE);
            v.setVisibility(View.INVISIBLE);
            mainActivity_viewModel.removeChoiceAddToOptions(tag);
            //flowLayout.removeView(v);
        }*/



    }
}

package com.example.anil.activitytracker.activities;

import android.app.ActivityManager;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.anil.activitytracker.R;
import com.example.anil.activitytracker.SharedPreferences_StoringChoices;
import com.example.anil.activitytracker.database.DailyActivityTime;
import com.example.anil.activitytracker.viewmodel.SetUp_Activity_ViewModel;

import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class SetUp_Activity extends AppCompatActivity {

    private HashSet<String> choiceHashSet;
    private HashMap<String,Integer> choiceViewIdHashmap;
    private String TAG = "SETUP_ACTIVITY";

    private SetUp_Activity_ViewModel viewModel ;
    private SharedPreferences_StoringChoices sharedPreferences_storingChoices;

    private LinearLayout parent_layout;
    private TextView timeLimitLabel;
    private DecimalFormat decimalFormat;
    private int backPressCount = 0;
    private double TOTAL_HOURS_IN_A_DAY = 24;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fill_activities_time);

        viewModel = ViewModelProviders.of(this).get(SetUp_Activity_ViewModel.class);
        parent_layout = findViewById(R.id.parent_layout);
        timeLimitLabel = findViewById(R.id.time_limit_label);
        choiceViewIdHashmap = new HashMap<>();

        sharedPreferences_storingChoices = new SharedPreferences_StoringChoices();

        Intent intent = getIntent();
        choiceHashSet = (HashSet<String>) intent.getSerializableExtra("choices");

        if (choiceHashSet != null) {
            Log.d(TAG, " choices = " + choiceHashSet.toString());
            //adding choices in sharedpreferences for future

            //adding "other" key to compensate and adjust 24 hrs format
            choiceHashSet.add("Other_Activities");

            sharedPreferences_storingChoices.storeChoices(choiceHashSet);
            //this will change the launch mode in splash activity
            sharedPreferences_storingChoices.storeLaunchBoolean(true);
            addSelectedChoices();
        } else {
            //user is coming second time , load from sharedpreferences
            choiceHashSet = sharedPreferences_storingChoices.getChoicesHashSet();
            if (choiceHashSet != null)
            {
                addSelectedChoices();
            }else {
                choiceHashSet = new HashSet<>();
                showToast("No activites to show.");
            }


        }
    }

    private void addSelectedChoices() {

        //checking if user had saved any data earlier
       viewModel.findDataByDate(new Date()).observe(this, new Observer<DailyActivityTime>() {
            @Override
            public void onChanged(@Nullable DailyActivityTime dailyActivityTime) {
                if ((dailyActivityTime != null) && (dailyActivityTime.activityHashMap != null))
                {
                    addChoicesWithValues(dailyActivityTime.activityHashMap);
                }
                else
                {
                    addChoicesWithValues(new HashMap<String, Double>());
                }
            }
        });

    }

    private void addChoicesWithValues(HashMap<String, Double> activityHashMap) {
        int id = 1;
        double time = 0;
        TOTAL_HOURS_IN_A_DAY = 24;
        parent_layout.removeAllViews();

        for (String s :choiceHashSet)
        {
            View view = LayoutInflater.from(this).inflate(R.layout.fill_activity_row_layout,null);
            TextView activity_name = view.findViewById(R.id.activity_name);
            EditText activity_time = view.findViewById(R.id.activity_time);

            activity_name.setText(s);
            activity_name.setTag(s);

            if (activityHashMap.get(s) != null)
            {
                time = roundOff(activityHashMap.get(s));
                TOTAL_HOURS_IN_A_DAY-= time;
                activity_time.setText("" + time);
            }

            activity_time.setId(id);
            choiceViewIdHashmap.put(s , id);
            id++;

            //hiding "other activities field from user
            if (s.equals("Other_Activities"))
            {
                view.setVisibility(View.GONE);
            }
            //add
            parent_layout.addView(view);
        }
    }

    public void onBtnClicked(final View view)
    {
        int id = view.getId();
        if (R.id.save_btn == id)
        {
            //convert into mins and then add
            HashMap<String,Double> hashMap = prepareChoiceHashmap();
            //insert record and checking whether it is in 24 hrs format or not
            if (TOTAL_HOURS_IN_A_DAY < 0 )
            {
                timeLimitLabel.setVisibility(View.VISIBLE);
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        timeLimitLabel.setVisibility(View.GONE);
                    }
                },1500);
            }
            else
            {
                viewModel.getDataRepository().insert(new DailyActivityTime(new Date(),hashMap));
                showToast("data saved ");
            }

        }
        else if(id == R.id.see_analysis_btn)
        {
            //check if the record is inserted
           //List<DailyActivityTime> dailyActivityTimeList =  viewModel.getAllData();
           //Log.d(TAG , "size = "+dailyActivityTimeList.size());

            viewModel.getAllData().observe(this, new Observer<List<DailyActivityTime>>() {
                @Override
                public void onChanged(@Nullable List<DailyActivityTime> dailyActivityTimes) {
                    Log.d(TAG , "size = "+dailyActivityTimes.size());
                }
            });

            //go to select analysis mode
            Intent  intent = new Intent(this, Select_Analysis_Mode_Activity.class);
            startActivity(intent);
        }
    }

    private HashMap<String, Double> prepareChoiceHashmap() {

        TOTAL_HOURS_IN_A_DAY = 24;
        double activityTime = 0;
        //convert into mins and then add
        HashMap<String,Double> hashMap = new HashMap<>();

        for (String choice : choiceHashSet)
        {
            EditText editText = findViewById(choiceViewIdHashmap.get(choice));
            if(!editText.getText().toString().equals(""))
            {
                activityTime = Double.parseDouble(editText.getText().toString());
            }
            else
            {
                activityTime = 0;
            }

            //activityTime = getMinFromHour(editText.getText().toString());
            TOTAL_HOURS_IN_A_DAY -= activityTime;
            hashMap.put(choice,activityTime );
        }
        //adjusting and putting extra hours in other activities to balance and maintain 24 hrs format
        hashMap.put("Other_Activities",TOTAL_HOURS_IN_A_DAY);

        return hashMap;
    }

    public double roundOff(double doubleValue)
    {
        decimalFormat = new DecimalFormat("#.##");
        decimalFormat.setRoundingMode(RoundingMode.CEILING);

        return Double.parseDouble(decimalFormat.format(doubleValue));
    }

    private Integer getMinFromHour(String s) {

        if (s.equals(""))
            return 0;

        int min = (int) (60 * Double.parseDouble(s));
        return min;
    }

    @Override
    public void onBackPressed() {

        if (++backPressCount < 2)
        {
            showToast(" Press Back again to Exit");
        }
        else
        {
            super.onBackPressed();
        }



    }

    public void showToast(String info) {
        Toast.makeText(this, info, Toast.LENGTH_SHORT).show();
    }
}

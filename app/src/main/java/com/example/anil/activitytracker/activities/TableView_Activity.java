package com.example.anil.activitytracker.activities;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.example.anil.activitytracker.CustomApplication;
import com.example.anil.activitytracker.R;
import com.example.anil.activitytracker.SharedPreferences_StoringChoices;
import com.example.anil.activitytracker.analysis_view_classes.PieChart;
import com.example.anil.activitytracker.database.DailyActivityTime;
import com.example.anil.activitytracker.viewmodel.PieChart_Activity_ViewModel;
import com.example.anil.activitytracker.viewmodel.TableView_Activity_ViewModel;

import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

public class TableView_Activity extends AppCompatActivity {

    private String TAG = "TABLE_VIEW_ACTIVITY";
    private SharedPreferences_StoringChoices sharedPreferences_storingChoices;

    private long fromTime, toTime;
    private TableView_Activity_ViewModel viewModel;
    private HashSet<String> totalChoiceHashSet;

    private TableLayout tableLayout;

    private int dataRequestCategory = -1;
    private static int rowCount = 0;
    private DecimalFormat decimalFormat;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_table_view_);

        init();

    }
    private void init() {

        tableLayout = findViewById(R.id.table_layout);
        //setting viewModel
        viewModel = ViewModelProviders.of(this).get(TableView_Activity_ViewModel.class);
        sharedPreferences_storingChoices = new SharedPreferences_StoringChoices();
        totalChoiceHashSet = sharedPreferences_storingChoices.getChoicesHashSet();

        showLog(" total hash set "+totalChoiceHashSet.size());
        Intent intent = getIntent();
        fromTime = intent.getLongExtra("fromDate",-1);
        toTime = intent.getLongExtra("toDate",-1);
        dataRequestCategory = (int) intent.getIntExtra("dataRequestCategory",-1);

        makeDataRequest(fromTime,toTime,dataRequestCategory);

        showLog(fromTime+" time == "+toTime);
    }

    // 0 - single date request
    // 1 - between dates
    // 2 - means all data

    private void makeDataRequest(long fromTime, long toTime, final int dataRequestCategory) {

        Calendar calendar = Calendar.getInstance();

        switch (dataRequestCategory)
        {
            case 0 :
                //get single date data
                viewModel.findDataByDate(new Date(fromTime)).observe(this, new Observer<DailyActivityTime>() {
                    @Override
                    public void onChanged(@Nullable DailyActivityTime dailyActivityTime) {
                        if (dailyActivityTime != null)
                        {
                            prepareTable(dailyActivityTime);
                            viewModel.printActivityHashmap(dailyActivityTime.activityHashMap);
                            showLog("dataRequestCategory = "+dataRequestCategory+" key = date = "+dailyActivityTime.date);
                        }

                    }
                });
                break;

            case 1 :
                //get data between dates
                showLog(" from time validate = "+(new Date(fromTime)).toString());
                viewModel.getDataBetweenDates(new Date(fromTime),new Date(toTime)).observe(this, new Observer<List<DailyActivityTime>>() {
                    @Override
                    public void onChanged(@Nullable List<DailyActivityTime> dailyActivityTimes) {
                        if (dailyActivityTimes != null)
                        {
                            viewModel.printActivityHashmap(dailyActivityTimes);
                            prepareTable(dailyActivityTimes);
                            showLog(" "+dataRequestCategory+" key = date = "+ dailyActivityTimes.size());
                        }

                    }
                });
                break;

            case 2 :
                //get all data
                viewModel.getAllData().observe(this, new Observer<List<DailyActivityTime>>() {
                    @Override
                    public void onChanged(@Nullable List<DailyActivityTime> dailyActivityTimes) {
                        if (dailyActivityTimes != null)
                        {
                            prepareTable(dailyActivityTimes);
                            showLog(" "+dataRequestCategory+" key = date = "+ dailyActivityTimes.size());
                        }
                    }
                });
                break;
        }
    }

    public void prepareTable(DailyActivityTime dailyActivityTime)
    {
        addHeadingTemplate();
        addView(dailyActivityTime);
    }

    public double roundOff(double doubleValue)
    {
        decimalFormat = new DecimalFormat("#.##");
        decimalFormat.setRoundingMode(RoundingMode.CEILING);

        return Double.parseDouble(decimalFormat.format(doubleValue));
    }

    public void prepareTable(List<DailyActivityTime> dailyActivityTimes)
    {
        addHeadingTemplate();
        for(DailyActivityTime dailyActivityTime : dailyActivityTimes)
        {
            addView(dailyActivityTime);
        }
    }

    private void addView(DailyActivityTime dailyActivityTime)
    {
        String tempData = "";
        HashMap<String, Double> hashmap = dailyActivityTime.activityHashMap;
        showLog("================activity hashmap = "+dailyActivityTime.activityHashMap.toString());
        showLog(" choices = "+totalChoiceHashSet.toString());
        TableRow tableRow = createTableRow();

        TextView textView = createTextView();
        textView.setText(viewModel.fromDate(dailyActivityTime.date));
        tableRow.addView(textView);
        for (String key : totalChoiceHashSet)
        {
            TextView textView1 = createTextView();
            if (hashmap.get(key) != null)
            {
                textView1.setText(""+roundOff(hashmap.get(key)));
                tableRow.addView(textView1);
                showLog("textview added");
            }
            else {
                textView1.setText("no data");
                textView1.setVisibility(View.INVISIBLE);
                tableRow.addView(textView1);
            }


        }
        tableLayout.addView(tableRow);
    }

    private void addHeadingTemplate()
    {
        TableRow tableRow = createTableRow();
        TextView textView = createTextView();
        textView.setText("Activities->\nDates ");
        tableRow.addView(textView);
        for (String key : totalChoiceHashSet)
        {
            showLog(" key = "+key);
            TextView textView1 = createTextView();
            textView1.setText( key);
            tableRow.addView(textView1);
        }

        //adding it to parent
        tableLayout.addView(tableRow);

    }

    private TableRow createTableRow()
    {
        TableRow tableRow = new TableRow(this);
        TableRow.LayoutParams lp = new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT);
        tableRow.setLayoutParams(lp);
        return tableRow;
    }
    private TextView  createTextView()
    {
        //set the style and listener here
        TextView textView = new TextView(this);
        textView.setLayoutParams(new TableRow.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.MATCH_PARENT));
//        textView.setMaxLines(2);
//        textView.setEllipsize(TextUtils.TruncateAt.END);
        textView.setGravity(Gravity.CENTER_VERTICAL);
        textView.setPadding(5,5,5,5);
        textView.setTextSize(20);
        textView.setTypeface(Typeface.DEFAULT,Typeface.BOLD);
        textView.setTextColor(Color.BLACK);
        textView.setBackgroundResource(R.drawable.cell_shape);
        int padding = (int) viewModel.dpToPixal(5);
        textView.setPadding(padding,padding,padding,padding);
        return textView;
    }

    public  void showLog(String info)
    {
        Log.d(TAG, info);
    }


}

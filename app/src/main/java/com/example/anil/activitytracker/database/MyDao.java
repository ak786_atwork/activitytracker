package com.example.anil.activitytracker.database;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.TypeConverters;
import android.arch.persistence.room.Update;

import java.util.Date;
import java.util.List;

@Dao
public interface MyDao {

    @Query("SELECT * FROM dailyactivitytime")
    LiveData<List<DailyActivityTime>> getAll();

    @Query("SELECT * FROM dailyactivitytime WHERE date = :selectedDate LIMIT 1")
    LiveData<DailyActivityTime> findDataByDate(Date selectedDate);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertTiming(DailyActivityTime dailyActivityTime);

    @Update(onConflict = OnConflictStrategy.REPLACE)
    void updateTiming(DailyActivityTime dailyActivityTime);

    @Query("DELETE  FROM DailyActivityTime")
    void deleteAll();

    @Query("SELECT * FROM dailyactivitytime WHERE date BETWEEN (:minValue) AND (:maxValue)")
    LiveData<List<DailyActivityTime>> findDataBetweenDates(Date minValue, Date maxValue);
}

package com.example.anil.activitytracker.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.example.anil.activitytracker.R;
import com.example.anil.activitytracker.TagLayout;

import java.util.HashSet;

public class PreviewChoices_Activity extends AppCompatActivity implements View.OnClickListener {

    private HashSet<String> choiceHashSet;
    private String TAG = "PREVIEW_CHOICE_ACTIVITY";
    private TagLayout flowLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.preview_choices);

        flowLayout = findViewById(R.id.flowLayout);

        Intent intent = getIntent();
        choiceHashSet = (HashSet<String>) intent.getSerializableExtra("choices");

        if (choiceHashSet != null) {
            Log.d(TAG, " choices = " + choiceHashSet.toString());
            addSelectedChoices();
        } else {
            choiceHashSet = new HashSet<>();
        }
    }
    private void addSelectedChoices() {
        for (String s : choiceHashSet) {
            addTextView(s);
        }
    }

    private void addTextView(String s)
    {
        TextView textView = (TextView) LayoutInflater.from(PreviewChoices_Activity.this).inflate(R.layout.selected_choice_item_textview, null);
        textView.setText(s);
        textView.setTag(s);
        textView.setOnClickListener(PreviewChoices_Activity.this);

        flowLayout.addView(textView);
    }

    @Override
    public void onClick(View v) {

        //for the choice textviews only
        String tag_choice = (String) v.getTag();
        //remove this child view
        flowLayout.removeView(v);
        choiceHashSet.remove(tag_choice);
    }

    //for buttons only
    public void onBtnClicked(View view)
    {
        Intent intent = null;
        int id = view.getId();

        if (id == R.id.add_more_activities_btn)
        {
            //send to AddChoice_Activity with choices
            intent = new Intent(PreviewChoices_Activity.this, AddChoice_Activity.class);
        }
        else if (id == R.id.setup_btn)
        {

            //send to setup activity
            intent = new Intent(PreviewChoices_Activity.this, SetUp_Activity.class);
            //only this activity will be on back stack
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);

        }
        //set the data and launch
        //avoiding crash
        if (intent != null)
        {
            intent.putExtra("choices",choiceHashSet);
            startActivity(intent);
        }

    }

}

package com.example.anil.activitytracker;

import android.app.Application;

public  class CustomApplication extends Application {

    private static CustomApplication singleton;

    public static CustomApplication getInstance(){
        return singleton;
    }
    @Override
    public void onCreate() {
        super.onCreate();
        singleton = this;
    }
}

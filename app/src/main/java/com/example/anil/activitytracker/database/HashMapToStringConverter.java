package com.example.anil.activitytracker.database;

import android.arch.persistence.room.TypeConverter;
import android.util.Log;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class HashMapToStringConverter {


    @TypeConverter
    public  static String fromHashMap(HashMap<String, Double> hashMap)
    {
        if (hashMap == null)
            return null;
       /* Log.d("json " , " data = "+hashMap.toString());
        JSONObject jsonObject = new JSONObject(hashMap);
        Log.d("Json", " json data = "+ jsonObject.toString());*/

        StringBuilder stringBuilder = new StringBuilder();

        for (Map.Entry<String,Double> entry : hashMap.entrySet())
        {
            stringBuilder.append(entry.getKey()+"="+entry.getValue()+",");
        }

        String s = stringBuilder.substring(0,stringBuilder.length()-1);
        /*Log.d("json " , " data = "+stringBuilder.toString());
        Log.d("json " , " data = "+s);*/
        return s;
    }

    @TypeConverter
    public static HashMap<String,Double> fromString(String data)
    {
        if (data == null)
            return null;

        HashMap<String,Double> hashMap = new HashMap<>();
        String[] keyValuePairs = data.split(",");
        String[] tempEntry = null;
        for (String pair : keyValuePairs)
        {
            tempEntry = pair.split("=");
            hashMap.put(tempEntry[0],Double.parseDouble(tempEntry[1]));
        }
        //Log.d("hashmap  " , " data = "+hashMap.toString());
        return hashMap;
    }
}

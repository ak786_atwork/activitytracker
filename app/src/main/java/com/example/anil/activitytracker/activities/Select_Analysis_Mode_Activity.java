package com.example.anil.activitytracker.activities;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.example.anil.activitytracker.R;
import com.example.anil.activitytracker.analysis_view_classes.PieChart;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class Select_Analysis_Mode_Activity extends AppCompatActivity implements View.OnClickListener  {

    private EditText from_edittext, to_edittext;
    private SimpleDateFormat simpleDateFormat;

    private RadioGroup radioGroup, radioGroupDateSelector;
    private RadioButton tableViewRadioBtn, pieChartViewRadioBtn;

    private Button viewBtn;
    private TextView dateErrorLabel;

    private long fromTime,toTime,singleDateTime;
    private int dataRequestCategory = -1;

    private final long YESTERDAY_LONG = 24 * 60 * 60 * 1000, PAST_SEVEN_LONG = 7 * 24 * 60 * 60 * 1000, PAST_FIFTEEN_LONG =15 * 24 * 60 * 60 * 1000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.select_analysis_mode);

        init();

    }

    private void init() {

        radioGroupDateSelector = findViewById(R.id.radio_group_date_selector);

        from_edittext = findViewById(R.id.from_date);
        to_edittext = findViewById(R.id.to_date);

        dateErrorLabel = findViewById(R.id.date_error_label);
        radioGroup = findViewById(R.id.radio_group_view_selector);
        tableViewRadioBtn = findViewById(R.id.table_view_radio_btn);
        pieChartViewRadioBtn = findViewById(R.id.pie_view_radio_btn);
        viewBtn = findViewById(R.id.view_btn);

        viewBtn.setOnClickListener(this);
        from_edittext.setOnClickListener(this);
        to_edittext.setOnClickListener(this);



        simpleDateFormat = new SimpleDateFormat();
    }

    @Override
    public void onClick(View v) {

        Intent intent;
        int id = v.getId();
        long[] dates ;
        switch (id) {
            case R.id.view_btn:
                //
                if (pieChartViewRadioBtn.isChecked()) {
                    //launch pie activity

                    intent = new Intent(this, PieChart_Activity.class);
                } else {
                    //launch table activity
                    intent = new Intent(this, TableView_Activity.class);
                }

                String fromDate = from_edittext.getText().toString();
                String toDate = to_edittext.getText().toString();
                if (!fromDate.equals("") && !toDate.equals(""))
                {
                   // dateErrorLabel.setVisibility(View.GONE);
                    intent.putExtra("fromDate",fromTime);
                    intent.putExtra("toDate",toTime);
                    intent.putExtra("dataRequestCategory", 1);

                }
                else
                {
                    //show label to show error
                    //dateErrorLabel.setText("Date can't be blank.");
                    //dateErrorLabel.setVisibility(View.VISIBLE);

                    //check for the radiobuttons input
                    dates = checkForRadioButton();
                    intent.putExtra("fromDate",dates[0]);
                    intent.putExtra("toDate",dates[1]);
                    intent.putExtra("dataRequestCategory", (int)dates[2]);
                }

                startActivity(intent);

                break;


            case R.id.from_date:
                //
                DatePickerDialog datePickerDialog = new DatePickerDialog(Select_Analysis_Mode_Activity.this);
                datePickerDialog.setOnDateSetListener(new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {

                        from_edittext.setText(dayOfMonth + "/" + ++month + "/" + year);
                        //fromTime = (new Date(year,month-1,dayOfMonth)).getTime();
                        fromTime = getDate(year,month-1,dayOfMonth).getTime();
                        Log.d("Date", " year " + year + " " + month + " " + dayOfMonth + " month" + view.getMonth());
                    }
                });
                datePickerDialog.show();
                break;

            case R.id.to_date:
                //
                DatePickerDialog datePickerDialog1 = new DatePickerDialog(Select_Analysis_Mode_Activity.this);
                datePickerDialog1.setOnDateSetListener(new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {

                        to_edittext.setText(dayOfMonth + "/" + ++month + "/" + year);
                        toTime = getDate(year,month-1,dayOfMonth).getTime();
                        Log.d("Date", " year " + year + " " + month + " " + dayOfMonth + " month" + view.getMonth());
                    }
                });
                datePickerDialog1.show();
                break;
        }
    }

    private Date getDate(int year,int month,int date)
    {
        Calendar calendar = Calendar.getInstance();
        calendar.set(year,month,date);
        return calendar.getTime();
    }

    private long[] checkForRadioButton() {

        long[] longDates = new long[3];
        int id = radioGroupDateSelector.getCheckedRadioButtonId();
        int dayOfMonth;
        Calendar calendar = Calendar.getInstance();
        long today = calendar.getTimeInMillis();


        switch (id)
        {
            case R.id.yesterday_radiobtn:
                //

                longDates[0] = today - YESTERDAY_LONG;
                longDates[2] = 0;
                return longDates;

            case R.id.past_seven_radiobtn:
                //as we need to avoid including today's date
                longDates[0] = today - PAST_SEVEN_LONG ;
                longDates[1] = today - YESTERDAY_LONG;
                longDates[2] = 1;
                return longDates;

            case R.id.past_fifteen_radiobtn:
                //
                longDates[0] = today - PAST_FIFTEEN_LONG ;
                longDates[1] = today - YESTERDAY_LONG;
                longDates[2] = 1;
                return longDates;

            case R.id.past_month_radiobtn:
                //

                int day = calendar.get(Calendar.DAY_OF_MONTH);
                int month =calendar.get(Calendar.MONTH);
                int year = calendar.get(Calendar.YEAR);
                if (month == 0)
                {
                    //set to december
                    year--;
                    month = 11;
                }
                else
                {
                    month = month-1;
                }
                //set from date
                //this date method is deprecated so it may create problem in future
                calendar.set(year,month,1);
                longDates[0] = calendar.getTimeInMillis();
                Toast.makeText(this,"day = "+calendar.getMaximum(Calendar.DAY_OF_MONTH),Toast.LENGTH_SHORT).show();
                calendar.set(year,month,calendar.getMaximum(Calendar.DAY_OF_MONTH));
                longDates[1] = calendar.getTimeInMillis();
                longDates[2] = 1;
                return longDates;

            case R.id.from_begining_radiobtn:
                //
                longDates[2] = 2;
                return longDates;

        }
        return longDates;
    }


}

// 0 - single date request
// 1 - between dates
// 2 - means all data


//Calendar.getInstance().getActualMaximum(Calendar.DAY_OF_MONTH);
//This returns actual maximum for current month. For example it is February of leap year now, so it returns 29 as int.
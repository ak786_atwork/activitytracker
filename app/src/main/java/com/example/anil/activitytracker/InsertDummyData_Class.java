package com.example.anil.activitytracker;

import android.util.Log;

import com.example.anil.activitytracker.database.DailyActivityTime;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Random;

public class InsertDummyData_Class {

    public static List<DailyActivityTime> prepareDummyList(HashSet<String> choices)
    {
        showLog("all choices "+choices.toString());

        List<DailyActivityTime> list = new ArrayList<>();

        Calendar calendar = Calendar.getInstance();
        int date = 4;
        int year =  calendar.get(Calendar.YEAR);
        int month =  calendar.get(Calendar.MONTH) ;
        double TOTAL_HOURS_IN_A_DAY = 24;
        double activityTime = 0;

        for (int i =0 ; i<31; i++)
        {
            TOTAL_HOURS_IN_A_DAY = 24;

            HashMap<String , Double> hashMap = new HashMap<>();
            for (String choice : choices)
            {
                if (TOTAL_HOURS_IN_A_DAY > 5 && !choice.equals("Other_Activities"))
                {
                    activityTime = getRandomHours();
                    TOTAL_HOURS_IN_A_DAY -= activityTime;
                }
                hashMap.put(choice, activityTime);
                activityTime = 0;
            }
            //adjusting remaining hours to maintain the format
            hashMap.put("Other_Activities",TOTAL_HOURS_IN_A_DAY);

            Date date1 = getDate(year,month,date--); //new Date(year,month,date--);
            showLog(date1.toString());
            list.add(new DailyActivityTime(date1,hashMap));
            if (date <= 0)
            {
                date = 31;
                month--;
            }
        }
        showLog("list size = "+list.size());
        return list;
    }

    private static double getRandomHours()
    {
        Random random = new Random();
        return  5 * random.nextDouble();
    }

/*    private static int getRandomMinutes()
    {
        Random random = new Random();
        return  random.nextInt(100);
    }*/

    private static Date getDate(int year,int month,int day)
    {
        Calendar calendar = Calendar.getInstance();
        calendar.set(year,month,day);
        return calendar.getTime();
    }

    private static void showLog(String info)
    {
        Log.d("INSERT_DUMMY_CLASS",info);
    }
}

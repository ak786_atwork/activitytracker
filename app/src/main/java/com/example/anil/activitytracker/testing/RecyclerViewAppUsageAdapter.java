package com.example.anil.activitytracker.testing;

import android.app.usage.UsageStats;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.Layout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.anil.activitytracker.R;

import java.util.List;

public class RecyclerViewAppUsageAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private  List<UsageStats> usageStatsList;

    public RecyclerViewAppUsageAdapter(List<UsageStats> usageStatsList)
    {
        this.usageStatsList = usageStatsList;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.app_usage_recycler_row_item,null);
        ItemViewHolder itemViewHolder = new ItemViewHolder(view);

        /*if (viewGroup == null)
        {
            itemViewHolder = new ItemViewHolder(view);
            viewGroup.setTag(itemViewHolder);
        }
        else
        {
            itemViewHolder = (ItemViewHolder) viewGroup.getTag();
        }*/


        return itemViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int i) {


        ((ItemViewHolder)viewHolder).textViewAppName.setText(usageStatsList.get(i).getPackageName());
        ((ItemViewHolder)viewHolder).textViewAppUsageTime.setText(getHoursFromMillis(usageStatsList.get(i).getTotalTimeInForeground()) + "");
    }

    @Override
    public int getItemCount() {
        return usageStatsList.size();
    }

    public double getHoursFromMillis(long timeInMillis)
    {
        return (double) timeInMillis/(1000 *60 * 60);
    }

    public class ItemViewHolder extends RecyclerView.ViewHolder
    {

        public TextView textViewAppName;
        public TextView textViewAppUsageTime;
        public ItemViewHolder(@NonNull View itemView) {
            super(itemView);

            textViewAppName = itemView.findViewWithTag("application_name");
            textViewAppUsageTime = itemView.findViewWithTag("application_usage_time");
        }
    }
}

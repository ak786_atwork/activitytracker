package com.example.anil.activitytracker.viewmodel;

import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;
import android.content.Intent;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.example.anil.activitytracker.R;
import com.example.anil.activitytracker.TagLayout;
import com.example.anil.activitytracker.activities.AddChoice_Activity;
import com.example.anil.activitytracker.activities.MainActivity;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class MainActivity_ViewModel extends ViewModel implements View.OnClickListener{

    private String TAG = "MainActivity_ViewModel";
    MutableLiveData<ViewStateHolder> selectedChoiceMutable = new MutableLiveData<>();
    Map<String,Integer> addedChoiceMap = new HashMap<>();

    ViewStateHolder viewStateHolder = new ViewStateHolder();

    public MutableLiveData<ViewStateHolder> getSelectedChoice() {
        return selectedChoiceMutable;
    }

    //from here
    Map<Integer,ViewStateHolder> viewStateHolderMap = new HashMap<>();

    public void onClick(View view)
    {
        Log.d(TAG, "onclick in main view model");
        int id = view.getId();
        String key = ((TextView) view).getText().toString();

        ViewStateHolder viewStateHolder = new ViewStateHolder();
        //preparing viewstate holder object
        viewStateHolder.key = key;
        viewStateHolder.id  = id;
        viewStateHolder.parentFlowLayout = (TagLayout) view.getParent();

        viewStateHolderMap.put(id,viewStateHolder);
        selectedChoiceMutable.postValue(viewStateHolder);
        ((TagLayout) view.getParent()).removeView(view);
/*
        boolean addedInMap = false;
        if (addedChoiceMap.get(key) != null)
        {
            addedInMap = true;
        }

        switch (id)
        {
            case R.id.cricket:
                //
                Log.d(TAG,"id == "+R.id.cricket);
                addedChoiceMap.put(key,R.id.cricket);
                Log.d(TAG, "cricket clicked "+addedChoiceMap.size());
                break;

            case R.id.hockey:
                //
                addedChoiceMap.put(key,R.id.hockey);
                Log.d(TAG, "hockeyclicked");
                break;

            case R.id.badminton:
                //
                addedChoiceMap.put(key,R.id.badminton);
                Log.d(TAG, "badmintonclicked");
                break;

            case R.id.wrestling:
                //
                addedChoiceMap.put(key,R.id.wrestling);
                Log.d(TAG, "wrestlingclicked");
                break;

            case R.id.carrom:
                //
                addedChoiceMap.put(key,R.id.carrom);
                Log.d(TAG, "carromclicked");
                break;

            case R.id.table_tenis:
                //
                addedChoiceMap.put(key,R.id.table_tenis);
                Log.d(TAG, "table_tenisclicked");
                break;

            case R.id.gym:
                //
                addedChoiceMap.put(key,R.id.gym);
                Log.d(TAG, "gymclicked");
                break;

            case R.id.martial_arts:
                //
                addedChoiceMap.put(key,R.id.martial_arts);
                Log.d(TAG, "martial_artsclicked");
                break;

            case R.id.aerobic:
                //
                addedChoiceMap.put(key,R.id.aerobic);
                Log.d(TAG, "aerobicclicked");
                break;

            case R.id.yoga:
                //
                addedChoiceMap.put(key,R.id.yoga);
                Log.d(TAG, "yogaclicked");
                break;

            case R.id.kabbadi:
                //
                addedChoiceMap.put(key,R.id.kabbadi);
                Log.d(TAG, "kabbadiclicked");
                break;

            case R.id.fb:
                //
                addedChoiceMap.put(key,R.id.fb);
                Log.d(TAG, "fbclicked");
                break;

            case R.id.whatsapp:
                //
                addedChoiceMap.put(key,R.id.whatsapp);
                Log.d(TAG, "whatsappclicked");
                break;

            case R.id.twitter:
                //
                addedChoiceMap.put(key,R.id.twitter);
                Log.d(TAG, "twitterclicked");
                break;

            case R.id.snapchat:
                //
                addedChoiceMap.put(key,R.id.snapchat);
                Log.d(TAG, "snapchatclicked");
                break;

            case R.id.hike:
                //
                addedChoiceMap.put(key,R.id.hike);
                Log.d(TAG, "hikeclicked");
                break;

            case R.id.instagram:
                //
                addedChoiceMap.put(key,R.id.instagram);
                Log.d(TAG, "instagramclicked");
                break;

            case R.id.telegram:
                //
                addedChoiceMap.put(key,R.id.telegram);
                Log.d(TAG, "telegramclicked");
                break;

            case R.id.novel:
                //
                addedChoiceMap.put(key,R.id.novel);
                Log.d(TAG, "novelclicked");
                break;

            case R.id.newspaper:
                //
                addedChoiceMap.put(key,R.id.newspaper);
                Log.d(TAG, "newspaperclicked");
                break;

            case R.id.magazine:
                //
                addedChoiceMap.put(key,R.id.magazine);
                Log.d(TAG, "magazineclicked");
                break;

            case R.id.comics:
                //
                addedChoiceMap.put(key,R.id.comics);
                Log.d(TAG, "clicked");
                break;

            case R.id.stories:
                //
                addedChoiceMap.put(key,R.id.stories);
                Log.d(TAG, "comicsclicked");
                break;

            case R.id.literature:
                //
                addedChoiceMap.put(key,R.id.literature);
                Log.d(TAG, "literatureclicked");
                break;

            case R.id.case_study:
                //
                addedChoiceMap.put(key,R.id.case_study);
                Log.d(TAG, "case_studyclicked");
                break;

            case R.id.self_study:
                //
                addedChoiceMap.put(key,R.id.self_study);
                Log.d(TAG, "self_studyclicked");
                break;

            case R.id.gossip:
                //
                addedChoiceMap.put(key,R.id.gossip);
                Log.d(TAG, "gossipclicked");
                break;

            case R.id.discussion:
                //
                addedChoiceMap.put(key,R.id.discussion);
                Log.d(TAG, "discussionclicked");
                break;

            case R.id.self_talk:
                //
                addedChoiceMap.put(key,R.id.self_talk);
                Log.d(TAG, "self_talkclicked");
                break;

            case R.id.mobile_games:
                //
                addedChoiceMap.put(key,R.id.mobile_games);
                Log.d(TAG, "mobile_gamesclicked");
                break;

            case R.id.self_analysis:
                //
                addedChoiceMap.put(key,R.id.self_analysis);
                Log.d(TAG, "self_analysisclicked");
                break;

            case R.id.trip:
                //
                addedChoiceMap.put(key,R.id.trip);
                Log.d(TAG, "            case R.id.trip:");
                break;

            case R.id.usual_travelling:
                //
                addedChoiceMap.put(key,R.id.usual_travelling);
                Log.d(TAG, "usual_travelling");
                break;

            case R.id.work_hours:
                //
                addedChoiceMap.put(key,R.id.work_hours);
                Log.d(TAG, "work_hours");
                break;

            case R.id.breakfast:
                //
                addedChoiceMap.put(key,R.id.breakfast);
                Log.d(TAG, "breakfast");
                break;

            case R.id.lunch:
                //
                addedChoiceMap.put(key,R.id.lunch);
                Log.d(TAG, "lunch");
                break;

            case R.id.dinner:
                //
                addedChoiceMap.put(key,R.id.dinner);
                Log.d(TAG, "dinner");
                break;

            case R.id.sleep:
                //
                addedChoiceMap.put(key,R.id.sleep);
                Log.d(TAG, "dinner");
                break;

            default:
                //
                break;

        }
        //now add view into selected choice portion


        viewStateHolder.addedInMap = addedInMap;
        viewStateHolder.key = key;*/
        //selectedChoiceMutable.postValue(viewStateHolder);
        //view.setVisibility(View.INVISIBLE);



    }

    public ViewStateHolder getViewStateHolderById(int id) {
        return viewStateHolderMap.remove(id);
        //return viewStateHolderMap.get(id);
    }

    public void removeChoiceAddToOptions(String key)
    {
        //remove from choicemap

        addedChoiceMap.remove(key);


    }

    public int getViewId(String key)
    {
        for(Map.Entry<String,Integer> entry : addedChoiceMap.entrySet())
        {
            Log.d(TAG, "key = "+entry.getKey() +"  value == "+entry.getValue());
        }
        Log.d(TAG,key+" == "+addedChoiceMap.get(key));
        return addedChoiceMap.get(key) == null ? -1 : addedChoiceMap.get(key);
    }

    public Set<String> getListOfChoices()
    {
       // return addedChoiceMap.keySet();

        Set<String> set = new HashSet<>();
        for (Map.Entry<Integer,ViewStateHolder> entry : viewStateHolderMap.entrySet())
        {
            set.add(entry.getValue().key);
        }
        return set;
    }

    public static class ViewStateHolder
    {
        public String key;
        public TagLayout parentFlowLayout;
        public int id;
        public boolean addedInMap;
    }
}

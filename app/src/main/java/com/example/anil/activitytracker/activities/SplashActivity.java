package com.example.anil.activitytracker.activities;

import android.arch.lifecycle.Observer;
import android.content.Intent;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import com.example.anil.activitytracker.InsertDummyData_Class;
import com.example.anil.activitytracker.R;
import com.example.anil.activitytracker.SharedPreferences_StoringChoices;
import com.example.anil.activitytracker.database.DailyActivityTime;
import com.example.anil.activitytracker.repository.DataRepository;

import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

import static com.example.anil.activitytracker.InsertDummyData_Class.prepareDummyList;

public class SplashActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        /*new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                decideAndLaunchActivity();
            }
        },2000);*/

        decideAndLaunchActivity();
        //check to get data between dates
        /*DataRepository dataRepository = new DataRepository();
        dataRepository.getDataBetweenDates(getDate(2019,0,12),getDate(2019,0,22)).observe(this, new Observer<List<DailyActivityTime>>() {
            @Override
            public void onChanged(@Nullable List<DailyActivityTime> dailyActivityTimes) {
                showLog("size bw dates = "+dailyActivityTimes.size());
            }
        });*/

    }

    private Date getDate(int year,int month,int date)
    {
        Calendar calendar = Calendar.getInstance();
        calendar.set(year,month,date);
        return calendar.getTime();
    }



    public void decideAndLaunchActivity()
    {
        SharedPreferences_StoringChoices sharedPreferences_storingChoices = new SharedPreferences_StoringChoices();
        boolean choicesFinalized = sharedPreferences_storingChoices.getLaunchBoolean();

      /*  DataRepository dataRepository = new DataRepository();
        //deleting all data
       dataRepository.deleteAll();

        //this is to insert dummy data
        HashSet<String> choices = sharedPreferences_storingChoices.getChoicesHashSet();
        List<DailyActivityTime> list = InsertDummyData_Class.prepareDummyList(choices);

        for (DailyActivityTime dailyActivityTime : list)
        {
            showLog("inserting data");
            dataRepository.insert(dailyActivityTime);
        }*/

      //showLog(" all choices from sharedpreferences = "+sharedPreferences_storingChoices.getChoicesHashSet().toString());
        Intent intent;

        if (choicesFinalized)
        {
            //launch setup activity
            intent = new Intent(this,SetUp_Activity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        }
        else
        {
            //choices yet to make
            intent = new Intent(this, MainActivity.class);
        }
        startActivity(intent);

    }

    private void showLog(String info)
    {
        Log.d("INSERT_DUMMY_DATA",info);
    }
}

package com.example.anil.activitytracker.activities;

import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.anil.activitytracker.R;
import com.example.anil.activitytracker.TagLayout;
import com.example.anil.activitytracker.viewmodel.AddChoice_Activity_ViewModel;

import java.util.HashSet;

public class AddChoice_Activity extends AppCompatActivity implements View.OnClickListener {

    private String TAG = "ADD_CHOICE_ACTIVITY";
    private TagLayout flowLayout;
    private HashSet<String> choiceHashSet;
    private EditText choiceEditText;
    private TextView activity_created_label;

    private AddChoice_Activity_ViewModel viewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_more_activites);

        flowLayout = findViewById(R.id.flowLayout);
        choiceEditText = findViewById(R.id.activity_edittext);
        activity_created_label = findViewById(R.id.activity_created_label);
        viewModel = ViewModelProviders.of(this).get(AddChoice_Activity_ViewModel.class);

        Intent intent = getIntent();
        choiceHashSet = (HashSet<String>) intent.getSerializableExtra("choices");

        if (choiceHashSet != null) {
            Log.d(TAG, " choices = " + choiceHashSet.toString());
            addSelectedChoices();
        } else {
            choiceHashSet = new HashSet<>();
        }

    }

    private void addSelectedChoices() {
        for (String s : choiceHashSet) {
            addTextView(s);
        }
    }

    private void addTextView(String s)
    {
        TextView textView = (TextView) LayoutInflater.from(AddChoice_Activity.this).inflate(R.layout.selected_choice_item_textview, null);
        textView.setText(s);
        textView.setTag(s);
        textView.setOnClickListener(AddChoice_Activity.this);

        flowLayout.addView(textView);
    }

    @Override
    public void onClick(View v) {

        //for the choice textviews only
        String tag_choice = (String) v.getTag();
        //remove this child view
        flowLayout.removeView(v);
        choiceHashSet.remove(tag_choice);
    }

    public void onBtnClicked(View view) {
        int id = view.getId();
        Intent intent;
        switch (id) {
            case R.id.preview_btn:
                //
                intent = new Intent(AddChoice_Activity.this, PreviewChoices_Activity.class);
                intent.putExtra("choices", choiceHashSet);
                startActivity(intent);
                break;

            case R.id.setup_btn:
                //
                intent = new Intent(AddChoice_Activity.this, SetUp_Activity.class);
                //only this activity will be on back stack
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                intent.putExtra("choices", choiceHashSet);
                startActivity(intent);
                break;

            case R.id.create_activity_btn:
                //
                String choice = choiceEditText.getText().toString();
                if (choice.equals("")) {
                    //show toast and update textview label
                    showToast("Activity can't be blank.");
                    activity_created_label.setText("Activity can't be blank.");

                }
                else
                {
                    //add choice to flow layout and hashset
                    addTextView(choice);
                    choiceHashSet.add(choice);
                    choiceEditText.setText("");
                    showToast("Activity created.");
                    activity_created_label.setText("Activity created.");

                }
                break;

            default:
                //
                Log.d(TAG, "default switch");
                break;

        }
    }

    public void showToast(String info) {
        Toast.makeText(this, info, Toast.LENGTH_SHORT).show();
    }
}

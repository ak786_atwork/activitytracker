package com.example.anil.activitytracker.testing;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.example.anil.activitytracker.R;
import com.example.anil.activitytracker.database.DailyActivityTime;

import java.util.Date;
import java.util.HashMap;
import java.util.List;

public class TableLayout_Testing_Activity extends AppCompatActivity {

    private TableLayout tableLayout;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_table_layout__testing_);

        tableLayout = findViewById(R.id.table_layout);
        /*for (int i = 0; i < 5; i++) {

            TableRow row = new TableRow(this);
            TableRow.LayoutParams lp = new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT);
            row.setLayoutParams(lp);
            TextView textView = new TextView(this);
            textView.setText("Name" + " " + i);
            row.addView(textView);
            tableLayout.addView(row);
        }*/

        prepareTable();
    }
    public void prepareTable()
    {
        addHeadingTemplate();
     /*   for (int i = 0;i<5;i++)
        {
            addView(i);
        }*/
    }


    private void addView(int viewNumber)
    {
        String tempData = "";
       // HashMap<String, Integer> hashmap = dailyActivityTime.activityHashMap;
       // showLog("activity hashmap = "+dailyActivityTime.activityHashMap.size());
        TableRow tableRow = createTableRow();

        TextView textView = createTextView();
        textView.setText((new Date()).toString());
        tableRow.addView(textView);
        for (int i =0 ; i<viewNumber;i++)
        {
            TextView textView1 = createTextView();

                textView1.setText(""+viewNumber);
                tableRow.addView(textView1);
                //showLog("textview added");




        }
        tableLayout.addView(tableRow);
    }

    private void addHeadingTemplate()
    {
        TableRow tableRow = createTableRow();
        TextView textView = createTextView();
        textView.setText("Dates "+""+"Activities");
        tableRow.addView(textView);
        for (int i =0 ;i<5;i++)
        {
            TextView textView1 = createTextView();
            textView1.setText(" count "+i);
            tableRow.addView(textView1);
        }

        //adding it to parent
        tableLayout.addView(tableRow);

    }

    private TableRow createTableRow()
    {
        TableRow tableRow = new TableRow(this);
        TableRow.LayoutParams lp = new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT);
        tableRow.setLayoutParams(lp);
        return tableRow;
    }
    private TextView  createTextView()
    {
        //set the style and listener here
        TextView textView = new TextView(this);
        //textView.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
//        textView.setWidth(ViewGroup.LayoutParams.WRAP_CONTENT);
//        textView.setHeight(ViewGroup.LayoutParams.WRAP_CONTENT);
        return textView;
    }
}

package com.example.anil.activitytracker.testing;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.example.anil.activitytracker.R;

public class ShowInstalledAppsActivity extends AppCompatActivity {

    private RecyclerView recyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_installed_apps);

        recyclerView = findViewById(R.id.recycler_view_show_install_apps);
        recyclerView.setLayoutManager( new LinearLayoutManager(this));
        recyclerView.setHasFixedSize(true);

//        recyclerView.setAdapter(new ShowInstallAppsAdapter(new AppUsage().getAllInstalledApps()));
        recyclerView.setAdapter(new ShowInstallAppsAdapter(new AppUsage().getMainApps()));
    }
}

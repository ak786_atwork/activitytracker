package com.example.anil.activitytracker.testing;

import android.app.Application;
import android.app.usage.UsageStats;
import android.app.usage.UsageStatsManager;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;

import com.example.anil.activitytracker.CustomApplication;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import static android.content.Context.USAGE_STATS_SERVICE;

public class AppUsage {

    private Application applicationContext;
    private UsageStatsManager usageStatsManager;
    public AppUsage()
    {
        applicationContext = CustomApplication.getInstance();
    }
    public List<UsageStats> getUsageStatsList(int interval, long startTime, long endTime)
    {

        usageStatsManager = (UsageStatsManager) applicationContext.getSystemService(USAGE_STATS_SERVICE);
        List<UsageStats>  usageStatsList =  usageStatsManager.queryUsageStats(interval,startTime,endTime);

        return usageStatsList;
    }


    public List<PackageInfo> getAllInstalledApps()
    {
        PackageManager packageManager = applicationContext.getPackageManager();
        List<PackageInfo> packageInfoList = packageManager.getInstalledPackages(PackageManager.GET_META_DATA);

      /*  List<PackageInfo> modifiedPackageInfoList = new ArrayList<>();

        //to get only installed apps     Getting list of installed non-system apps
        for (int i =0 ;i< packageInfoList.size();i++)
        {
            if ( (packageInfoList.get(i).applicationInfo.flags & ApplicationInfo.FLAG_SYSTEM) == 0) {
                modifiedPackageInfoList.add(packageInfoList.get(i));
            }
        }


        return modifiedPackageInfoList;*/

        //getting system app as well
        return packageInfoList;
    }

    public List<ResolveInfo> getMainApps()
    {
        Intent mainIntent = new Intent(Intent.ACTION_MAIN,null);
        mainIntent.addCategory(Intent.CATEGORY_LAUNCHER);

        return applicationContext.getPackageManager().queryIntentActivities(mainIntent, 0);
    }
}

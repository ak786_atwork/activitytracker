package com.example.anil.activitytracker.testing;

import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.ResolveInfo;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.anil.activitytracker.CustomApplication;
import com.example.anil.activitytracker.R;

import java.util.List;

public class ShowInstallAppsAdapter extends RecyclerView.Adapter {
    List<PackageInfo> packageInfoList;
    CustomApplication customApplication ;
    List<ResolveInfo> appsList;

    public ShowInstallAppsAdapter(List<ResolveInfo> appsList) {
//        this.packageInfoList = packageInfoList;
        this.appsList = appsList;
        customApplication = CustomApplication.getInstance();
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.show_install_app_item_layout,null);
        v.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                toggleCheckboxState((CheckBox) v.findViewById(R.id.install_app_checkbox));
            }
        });
        return new ItemViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int i) {

        //this should be taken care
        ((ItemViewHolder)viewHolder).appName.setText(appsList.get(i).loadLabel(customApplication.getPackageManager()));
        ((ItemViewHolder)viewHolder).imageIcon.setImageDrawable(appsList.get(i).loadIcon(customApplication.getPackageManager()));
//            ((ItemViewHolder)viewHolder).imageIcon.setImageDrawable(packageInfoList.get(i).applicationInfo.loadIcon(customApplication.getPackageManager()));
//            ((ItemViewHolder)viewHolder).appName.setText(packageInfoList.get(i).applicationInfo.loadLabel(customApplication.getPackageManager()));


    }

    @Override
    public int getItemCount() {
        return appsList.size();
        //return packageInfoList.size();
    }


    public class ItemViewHolder extends RecyclerView.ViewHolder {

        public ImageView imageIcon;
        public CheckBox checkBox;
        public TextView appName;

        public ItemViewHolder(@NonNull View itemView) {
            super(itemView);

            imageIcon = itemView.findViewById(R.id.install_app_icon);
            appName = itemView.findViewById(R.id.install_app_name);
            checkBox = itemView.findViewById(R.id.install_app_checkbox);
        }
    }

    public void toggleCheckboxState(CheckBox checkBox)
    {
        if (checkBox.isChecked())
        {
            checkBox.setChecked(false);
        }
        else
        {
            checkBox.setChecked(true);
        }
    }
}

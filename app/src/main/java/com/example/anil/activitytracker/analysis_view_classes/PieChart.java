package com.example.anil.activitytracker.analysis_view_classes;

import android.app.Activity;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.hardware.display.DisplayManager;

import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.View;

import com.example.anil.activitytracker.activities.PieChart_Activity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Random;

public class PieChart extends View {

    private int radius = 0;
    private  Context context;
    private Canvas canvas;

    private ArrayList<PieChartHelper> pieChartHelperArrayList;

    public PieChart(Context context) {
        super(context);
        this.context =context;
    }

    public void addChoices(HashMap<String,Integer> hashMap)
    {
        showLog("add choice called");
    }

    public void setPieChartHelperArrayList(ArrayList<PieChartHelper> pieChartHelperArrayList)
    {
        this.pieChartHelperArrayList = pieChartHelperArrayList;
    }

    @Override
    protected void onDraw(Canvas canvas) {
        //this.canvas = canvas;

        showLog("ondraw called");

        Paint paint = new Paint();
        RectF square = getPieChartSize();
        float startAngle = 0;

        if (pieChartHelperArrayList != null)
        {
            showLog("arraylist size sent by viewmodel  = "+pieChartHelperArrayList.size());
            //iterate through all
            for (PieChartHelper helper : pieChartHelperArrayList)
            {
                paint.setColor(helper.color);
                canvas.drawArc(square,startAngle,helper.degree,true,paint);
                startAngle += helper.degree;
            }
        }


       /* paint.setColor(getRandomColor());



        canvas.drawArc(square,0,90,true,paint);
        paint.setColor(getRandomColor());

        canvas.drawArc(square,90,90,true,paint);
        paint.setColor(getRandomColor());

        canvas.drawArc(square,180,90,true,paint);
        paint.setColor(getRandomColor());

        canvas.drawArc(square,270,90,true,paint);
        paint.setColor(getRandomColor());*/

    }

    public void drawAgain()
    {
        showLog("draw again called");
        invalidate();
        //draw(new Canvas());
        //onDraw(canvas);
    }

    public RectF getPieChartSize()
    {
        int left, top, right ,bottom;
        top= 40;

        DisplayMetrics dm = new DisplayMetrics();
        ((Activity)context).getWindowManager().getDefaultDisplay().getMetrics(dm);
        int center = dm.widthPixels/2;
        RectF rectangle ;
        if (radius != 0)
        {
            left = center - radius;
            right = center + radius;
            bottom = top + 2 * radius;
        }
        else {
            radius = dm.widthPixels/3;
            left = center - radius;
            right = center + radius;
            bottom = top + 2 * radius;
        }

        return new RectF(left,top,right,bottom);
    }

    public HashSet<Integer> getRandomColor(int size)
    {
        int count = 0;
        HashSet<Integer> colorSet = new HashSet<>();

        while (count < size)
        {
            colorSet.add(getRandomColor());
            count = colorSet.size();
        }

        return colorSet;
    }

    public int getRandomColor()
    {
        Random random = new Random();
        int r = random.nextInt(255);
        int g = random.nextInt(255);
        int b = random.nextInt(255);

        return Color.rgb(r,g,b);
    }

    private void showLog(String info)
    {
        Log.d("PIE_CHART_CLASS" , info);
    }
}

package com.example.anil.activitytracker.testing;

import android.Manifest;
import android.app.usage.UsageStats;
import android.app.usage.UsageStatsManager;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.TextView;

import com.example.anil.activitytracker.R;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import static android.provider.Settings.ACTION_USAGE_ACCESS_SETTINGS;

public class AppUsageTestingActivity extends AppCompatActivity {

    private String TAG = "APP_USAGE_TESTING_ACTIVITY";
    private   TextView textView;
    private RecyclerView recyclerView;
    private RecyclerViewAppUsageAdapter recyclerViewAppUsageAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_app_usage_testing);

        textView = findViewById(R.id.textview);
        recyclerView = findViewById(R.id.recycler_view);





    }

    @Override
    protected void onResume() {
        super.onResume();

        long times[] = getFromTimeToTime();
        List<UsageStats> usageStatsList = (new AppUsage()).getUsageStatsList(UsageStatsManager.INTERVAL_DAILY,times[0],times[1]);

        if (usageStatsList.size() == 0)
        {
            Intent intent = new Intent();
            intent.setAction(ACTION_USAGE_ACCESS_SETTINGS);
            startActivity(intent);
            //requestPermissions(new String[]{Manifest.permission.PACKAGE_USAGE_STATS},1);
        }
        else
        {

            StringBuffer stringBuffer = new StringBuffer();
            for (UsageStats usageStats : usageStatsList)
            {
                stringBuffer.append("package name = "+usageStats.getPackageName() + "total time used = "+getHoursFromMillis(usageStats.getTotalTimeInForeground()) +"\n");
                showLog("package name = "+usageStats.getPackageName() + " "+usageStats.getLastTimeUsed() + "total time used = "+usageStats.getTotalTimeInForeground());
            }

            //textView.setText(stringBuffer.toString());
            recyclerViewAppUsageAdapter = new RecyclerViewAppUsageAdapter(usageStatsList);
            recyclerView.setLayoutManager(new LinearLayoutManager(this));
            recyclerView.setHasFixedSize(true);
            recyclerView.setAdapter(recyclerViewAppUsageAdapter);

        }
    }

    private void showLog(String info)
    {
        Log.d(TAG, info);
    }

    private long[] getFromTimeToTime()
    {
        long[] times = new long[2];
        Calendar calendar = Calendar.getInstance();
        calendar.set(2019,2,1);
        times[0] = calendar.getTimeInMillis();

        calendar.set(2019,2,3);
        times[1] = calendar.getTimeInMillis();

        return times;
    }

    public double getHoursFromMillis(long timeInMillis)
    {
        return (double) timeInMillis/(1000 *60 * 60);
    }
}
